FROM node:18.18.2-buster-slim
WORKDIR /app
RUN apt update && apt install -y supervisor
ENV LOGIO_SERVER_CONFIG_PATH='/app/server.json'
ENV LOGIO_FILE_INPUT_CONFIG_PATH='/app/file.json'
RUN npm install -g log.io log.io-file-input
USER 0
COPY supervisor.conf /etc/supervisor/conf.d
COPY file.json .
COPY server.json .
EXPOSE 6688
CMD ["/usr/bin/supervisord"]